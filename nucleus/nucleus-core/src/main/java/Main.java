import com.example.myschema.Cat;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Cat cat = new Cat();
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-context/app-config.xml");
        MessageSender heh = (MessageSender) context.getBean("messageSender");
        sayHiAFewTimes(heh);
    }

    private static void sayHiAFewTimes(final MessageSender heh) {
        for (int i = 0; i < 10; i++) {
            new Thread(){
                @Override
                public void run() {
                    while (true) {
                        heh.sendHi(Thread.currentThread().getName() + ": Sid");
                        try {
                            Thread.sleep(new Random().nextInt(2000));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        }
    }

}
