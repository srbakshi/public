import org.springframework.jms.core.JmsTemplate;

public class MessageSender {

    private JmsTemplate jmsTemplate;

    public MessageSender(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public void sendHi(String name) {
        this.jmsTemplate.convertAndSend("Hello " + name + "!");
    }
}
