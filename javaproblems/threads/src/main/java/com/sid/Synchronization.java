package com.sid;

public class Synchronization {

    public static void main(String[] args) throws InterruptedException {
        new Synchronization().launch();
    }

    private int a;
    private final Object o = new Object();
    private final Object o1 = o;

    private void launch() throws InterruptedException {

        Thread1 t1 = new Thread1();
        t1.start();
        Thread2 t2 = new Thread2();
        t2.start();

        t1.join();
        t2.join();

        System.out.println("Final value=" + a);
    }

    class Thread1 extends Thread {
        @Override
        public void run() {
            int i = 0;
            while (i++ < 100000) {
                synchronized (o) {
                    a++;
                }
            }
        }
    }


    class Thread2 extends Thread {

        @Override
        public void run() {
            int i = 0;
            while (i++ < 100000) {
                synchronized (o1) {
                    a = a + 2;
                }
            }
        }
    }
}
