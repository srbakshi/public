package com.sid;

import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SynchronousQueueBackedThreadPoolExecutor {
    public static void main(String[] args) {
        new SynchronousQueueBackedThreadPoolExecutor().launch();
    }

    private void launch() {


        ThreadPoolExecutor threadPoolExecutor =
                        new ThreadPoolExecutor(0, 5, 10L, TimeUnit.SECONDS, new SynchronousQueue<>(), Executors.defaultThreadFactory(), new MyPolicy());

        for(int i=0; i<10; i++) {
            System.out.println("ADDING NUMBER: " + i);
            threadPoolExecutor.execute(new MyRunnable());
        }



    }

    private class MyRunnable implements Runnable {
        volatile int counter = 0;
        @Override
        public void run() {
            while (counter < 20) {
                System.out.println(Thread.currentThread().getName() + ": " + (counter++));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private class MyPolicy implements RejectedExecutionHandler {
        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            try {
                executor.getQueue().put(r);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
