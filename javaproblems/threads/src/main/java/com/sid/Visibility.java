package com.sid;

public class Visibility {

    /**
     * Integer x is shared between 2 threads without synchronization. ValueModifier thread sees and updates its own
     * copy of the object and that's why the Onlooker thread does not have visibility of changes.
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        new Visibility().run();
    }

    private volatile boolean runThreads = true;

    private void run() throws InterruptedException {

        Integer x = 0;
        Object lock = new Object();
        Thread t1 = new Thread(new Onlooker(x, lock), "onlooker-thread");
        Thread t2 = new Thread(new ValueModifier(x, lock), "valuemodifier-thread");

        t1.start();
        t2.start();

        t1.join();
        t2.join();

    }

    class Onlooker implements Runnable {

        private Integer x;
        private final Object lock;

        public Onlooker(Integer x, Object lock) {
            this.x = x;
            this.lock = lock;
        }

        @Override
        public void run() {
            while (runThreads) {
                synchronized (lock) {
                    System.out.println(Thread.currentThread().getName() + " sees x=" + x);
                }
            }
        }
    }


    class ValueModifier implements Runnable {

        private Integer x;
        private final Object lock;

        public ValueModifier(Integer x, Object lock) {
            this.x = x;
            this.lock = lock;
        }

        @Override
        public void run() {
            synchronized (lock) {
                while (x < 10) {
                    x = x + 1;
                    System.out.println(Thread.currentThread().getName() + " sees x=" + x);
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    runThreads = false;
                }
            }
        }
    }


}
