package com.sid;

public class Visibility2 {
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 20; i++) {
            new Visibility2().launch();
        }
    }

    private int a = 0;
    private int b = 0;
    private int r1;
    private int r2;

    private void launch() throws InterruptedException {
        System.out.print("a=" + a + ", b=" + b + ", r1=" + r1 + ", r2=" + r2 + " ==========> ");
        Updater1 updater1 = new Updater1();
        updater1.start();
        Updater2 updater2 = new Updater2();
        updater2.start();

        updater1.join();
        updater2.join();

        System.out.println("a=" + a + ", b=" + b + ", r1=" + r1 + ", r2=" + r2);
    }

    class Updater1 extends Thread {
        @Override
        public void run() {
            a = 10;
            r1 = b;
        }
    }


    class Updater2 extends Thread {
        @Override
        public void run() {
            b = 20;
            r2 = a;
        }
    }

}
