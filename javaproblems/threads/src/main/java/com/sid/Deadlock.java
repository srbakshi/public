package com.sid;

public class Deadlock {

    public static void main(String[] args) throws InterruptedException {
        new Deadlock().launch();
    }

    private void launch() throws InterruptedException {
        NamedLock a = new NamedLock("Lock A");
        NamedLock b = new NamedLock("Lock B");
        Thread t1 = new Thread(new ABAcquirer(a, b));
        Thread t2 = new Thread(new ABAcquirer(b, a));
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        throw new AssertionError("Does not reach here. Deadlocked!");
    }

    class ABAcquirer implements Runnable {

        private final NamedLock a;
        private final NamedLock b;

        public ABAcquirer(NamedLock a, NamedLock b) {
            this.a = a;
            this.b = b;
        }

        @Override
        public void run() {
            synchronized (a) {
                System.out.println(Thread.currentThread().getName() + ":Acquired " + a.name);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + ":Acquiring " + b.name);
                synchronized (b) {
                    System.out.println(Thread.currentThread().getName() + ": Acquired B" + b.name);
                }
            }
        }
    }

    class NamedLock {
        String name;

        public NamedLock(String name) {
            this.name = name;
        }
    }

}


