package com.sid.producerconsumer;

import java.util.ArrayList;
import java.util.List;

public class Driver {


    public static void main(String[] args) throws InterruptedException {
        new Driver().run();
    }

    private void run() throws InterruptedException {

        ThreadSafeBlockingQueue<Integer> sharedQueue = new ThreadSafeBlockingQueue<>();
        Producer p1 = new Producer("Producer1", sharedQueue);
        Producer p2 = new Producer("Producer2", sharedQueue);
        Producer p3 = new Producer("Producer3", sharedQueue);
        Consumer c1 = new Consumer("Consumer1", sharedQueue);
        Consumer c2 = new Consumer("Consumer2", sharedQueue);
        Consumer c3 = new Consumer("Consumer3", sharedQueue);

        List<Thread> threads = startThreads(p1, c1, c2, c3);

        for (Thread thread : threads) {
            thread.join();
        }

    }

    private List<Thread> startThreads(Runnable... runnables) {
        List<Thread> ll = new ArrayList<>();
        for (Runnable r : runnables) {
            Thread t = new Thread(r);
            ll.add(t);
            t.start();
        }
        return ll;
    }
}
