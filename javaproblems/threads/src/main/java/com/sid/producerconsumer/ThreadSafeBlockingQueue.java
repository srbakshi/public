package com.sid.producerconsumer;

import java.util.LinkedList;

public class ThreadSafeBlockingQueue<T> {

    private final int MAX_SIZE = 10;
    private LinkedList<T> internalQueue = new LinkedList<>();

    public synchronized void queue(T data) {

        while (internalQueue.size() == MAX_SIZE) {
            try {
                System.out.println("Waiting: " + getName());
                this.wait();
                System.out.println("Notified: " + getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        internalQueue.push(data);

        System.out.println(getName() + ": " + data + " SIZE: " + internalQueue.size());
        this.notifyAll();
    }

    public synchronized T dequeue() {

        while (internalQueue.isEmpty()) {
            try {
                System.out.println("Waiting: " + getName());
                this.wait();
                System.out.println("Notified: " + getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        T pop = internalQueue.pop();
        System.out.println(getName() + ": " + pop + " SIZE: " + internalQueue.size());
        this.notifyAll();
        return pop;
    }

    public String getName() {
        return Thread.currentThread().getName();
    }
}
