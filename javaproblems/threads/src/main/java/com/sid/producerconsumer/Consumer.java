package com.sid.producerconsumer;

public class Consumer implements Runnable {

    private String name;
    private ThreadSafeBlockingQueue<Integer> sharedQueue;

    public Consumer(String name, ThreadSafeBlockingQueue<Integer> sharedQueue) {
        this.name = name;
        this.sharedQueue = sharedQueue;
    }

    @Override
    public void run() {
        Thread.currentThread().setName(name);
        while (true) {
            this.sharedQueue.dequeue();
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }
}
