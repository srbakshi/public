package com.sid.producerconsumer;

import org.joda.time.LocalTime;

public class Producer implements Runnable {

    private String name;
    private ThreadSafeBlockingQueue<Integer> sharedQueue;

    public Producer(String name, ThreadSafeBlockingQueue<Integer> sharedQueue) {
        this.name = name;
        this.sharedQueue = sharedQueue;
    }

    @Override
    public void run() {
        Thread.currentThread().setName(name);
        while (true) {
            int data = LocalTime.now().secondOfMinute().get();
            sharedQueue.queue(data);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }


    }
}
