package com.sid;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/*
TODO: Fix?
 */
public class MultipleThreadsSequentialReadFromArray {

    public static void main(String[] args) throws InterruptedException {
        new MultipleThreadsSequentialReadFromArray().launch();
    }


    private void launch() throws InterruptedException {

        List<Thread> threads = startNThreads(5);

        for (Thread thread : threads) {
            thread.join();
        }
    }

    private List<Thread> startNThreads(int n) {

        LinkedList<Integer> seqList = createList();
        List<Thread> threads = new ArrayList<>();

        for(int i=0; i<n; i++) {
            Thread t = new Thread(new PopperRunnable(seqList));
            threads.add(t);
            t.start();
        }

        return threads;
    }

    private LinkedList<Integer> createList() {
        LinkedList<Integer> seqList = new LinkedList<>();
        for(int i=0; i<100; i++) {
            seqList.push(i);
        }
        return seqList;
    }

    class PopperRunnable implements Runnable {

        private final LinkedList<Integer> seqList;

        PopperRunnable(LinkedList<Integer> seqList) {
            this.seqList = seqList;
        }

        @Override
        public void run() {

            synchronized (this.seqList) {
                while (!this.seqList.isEmpty()) {
                    try {
                        Thread.sleep(new Random().nextInt(1000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Integer pop = this.seqList.pop();
                    System.out.println(Thread.currentThread().getName() + " popped: " + pop);
                    this.seqList.notifyAll();
                    try {
                        this.seqList.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                this.seqList.notifyAll();
            }



        }
    }

}
