package com.sid;

public class RaceCondition {

    int count = 0;
    private final Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {
        new RaceCondition().launch();
    }

    private void launch() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            count = 0;
            Thread t1 = new Thread(new Incrementer(), "t1");
            Thread t2 = new Thread(new Incrementer(), "t2");
            Thread t3 = new Thread(new Incrementer(), "t3");
            long st = System.currentTimeMillis();
            t1.start();
            t2.start();
            t3.start();
            t1.join();
            t2.join();
            t3.join();
            System.out.println(count + ": Time: " + (System.currentTimeMillis() - st));
            if (count != 150000) {
                System.out.println("Race condition detected.");
            }
        }

    }

    class Incrementer implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 50000; i++) {
                synchronized (lock) {
                    count++;
                }
            }
        }
    }


}
