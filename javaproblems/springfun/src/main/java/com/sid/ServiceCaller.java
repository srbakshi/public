package com.sid;

import org.springframework.beans.factory.annotation.Autowired;

public class ServiceCaller {

    @Autowired
    private Service service;

    public void call() {
        System.out.println(service.fetch());
    }


}
