package com.sid;

import com.sid.configuration.AppConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class AppBootstrap {
    public static void main(String[] args) {
        AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        ServiceCaller caller = (ServiceCaller) context.getBean("caller");
        caller.call();
    }
}
