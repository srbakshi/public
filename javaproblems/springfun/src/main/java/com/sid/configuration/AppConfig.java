package com.sid.configuration;

import com.sid.Service;
import com.sid.ServiceCaller;
import com.sid.ServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {


    @Bean
    public Service service() {
        return new ServiceImpl();
    }

    @Bean
    ServiceCaller caller() {
        return new ServiceCaller();
    }

}
